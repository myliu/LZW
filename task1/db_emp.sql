-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2016 年 11 月 17 日 01:42
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `db_emp`
--
CREATE DATABASE `db_emp` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db_emp`;

-- --------------------------------------------------------

--
-- 表的结构 `tb_emp`
--

CREATE TABLE IF NOT EXISTS `tb_emp` (
  `e_id` int(10) unsigned NOT NULL COMMENT 'ID',
  `e_name` varchar(20) NOT NULL COMMENT '姓名',
  `e_dept` varchar(20) NOT NULL COMMENT '所属部门',
  `e_img` varchar(50) NOT NULL COMMENT '头像',
  `date_of_birth` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '出生日期',
  `date_of_entry` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '入职时间',
  PRIMARY KEY (`e_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tb_emp`
--

INSERT INTO `tb_emp` (`e_id`, `e_name`, `e_dept`, `e_img`, `date_of_birth`, `date_of_entry`) VALUES
(1, '张三', '市场部', 'imagse/1.jpg', '2016-11-11 04:20:41', '2016-11-11 04:20:41'),
(2, '李四', '开发部', 'imagse/2.jpg', '2016-11-11 04:21:00', '2016-11-11 04:21:00'),
(3, '王五', '媒体部', 'imagse/3.jpg', '2016-11-11 04:21:10', '2016-11-11 04:21:10'),
(4, '马六', '销售部', 'imagse/4.jpg', '2016-11-11 04:21:16', '2016-11-11 04:21:16');
